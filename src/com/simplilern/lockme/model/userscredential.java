package com.simplilern.lockme.model;

public class userscredential {
	
	private String siteName;
	private String loggedUser;
	private String userName;
	private String passWord;
	
	
	public userscredential() {
		super();
		// TODO Auto-generated constructor stub
	}


	public userscredential(String siteName, String loggedUser, String userName, String passWord) {
		
		this.siteName = siteName;
		this.loggedUser = loggedUser;
		this.userName = userName;
		this.passWord = passWord;
	}


	


	public String getSiteName() {
		return siteName;
	}


	public  void setSiteName(String siteName) {
		this.siteName = siteName;
	}


	public String getLoggedUser() {
		return loggedUser;
	}


	public void setLoggedUser(String loggedUser) {
		this.loggedUser = loggedUser;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPassWord() {
		return passWord;
	}


	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}


	@Override
	public String toString() {
		return "userscredential [siteName=" + siteName + ", loggedUser=" + loggedUser + ", userName=" + userName
				+ ", passWord=" + passWord + "]";
	}


	
	

}
